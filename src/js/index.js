/**
 * Created by mavis on 2017/6/1.
 */

var groupIp = "https://mpmt.gemii.cc/";
var wxIp = "http://wx.gemii.cc/";

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return (r[2]);
    return null;
}